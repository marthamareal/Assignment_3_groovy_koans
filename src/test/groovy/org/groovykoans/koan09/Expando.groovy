package org.groovykoans.koan09

import org.groovykoans.koan12.Worker

/**
 * Created by marthamareal on 7/14/17.
 */
class Expando {

        def firstName
        def sayHello = {->
            return "Hello from ${firstName}"
        }

}
